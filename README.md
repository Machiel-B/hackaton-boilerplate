# Hackaton - WebVR - Deloitte Digital & Ambassadors

__[Forked from a-frame boilerplate](https://github.com/aframevr/aframe-boilerplate/)__

## Documentation
This project uses A-Frame, you can find the documentation and examplate at [https://aframe.io](https://aframe.io)

## Installation

To use this boilerplate, simply clone it;

```bash
$ git clone https://gitlab.com/AmbassadorsLab/hackathon-boilerplate
```

Install dependencies;

```bash
$ cd folder-name
$ npm install
```

Start the dev server;

```bash
$ npm start
```
Find out your lan ip to view on phone (port 3000);

```bash
$ ifconfig wlan0
```



*NOTE:*
- Not all phones / model nr's are represented in the online calibration database (dpdb).
- As a workaround you can add your own parameters in the config/dpdb.json file
- I.e. the **Samsung SM-G928I** is (for our purposes) the same as model as the **SM-G928F**, the latter was not in the db however...
